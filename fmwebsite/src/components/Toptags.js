import React, { Component } from "react";
import Tagsdesign from "../Design/tags";
import Axios from "axios";

 class Toptags extends Component {
  state = {
    toptags: [],
    current: 1
  };
  componentDidMount() {
    this.getTagsData();
  }
  getTagsData() {
    Axios.get(
      "http://ws.audioscrobbler.com/2.0/?method=chart.gettoptags&api_key=19aa554500134c46fb386b7dcef0f0b1&format=json&page=" +
        encodeURI(this.state.current)
    ).then(res => this.setState({ toptags: res.data.tags.tag }));
   
  }
  render() {
    return (
      <div className="mt-5">
        {this.state.toptags.map((i, key) => {
          return (
            <div key={key}>
              <Tagsdesign
                name={i.name}
                url={i.url}
                reach={i.reach}
                taggings={i.taggings}
              />
            </div>
          );
        })}
      </div>
    );
  }
}

export default Toptags;
