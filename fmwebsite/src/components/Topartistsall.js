import React, { Component } from "react";
import Axios from "axios";
import Card from "../Design/card";
import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";

 class Topartistsall extends Component {
  state = {
    alltopartists: [],
    current: 1
  };

  componentDidMount() {
    this.getArtistsData();
  }
  onChange = async page => {
    await this.setState({
      current: page
    });
    this.getArtistsData();
  };
  getArtistsData() {
    Axios.get(
      "http://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&api_key=19aa554500134c46fb386b7dcef0f0b1&format=json&limit=18&page=" +
        encodeURI(this.state.current)
    ).then(res => this.setState({ alltopartists: res.data.artists.artist }));
 
  }

  render() {
    return (
      <div className="main">
        <Pagination
          hrefBuilder="#top"
          className="pagination"
          onChange={this.onChange}
          current={this.state.current}
          total={5500}
        />
        <div className="container">
          <div className="row">
            {this.state.alltopartists.map((i, key) => {
              return (
                <div className="col-md-3" key={key}>
                  <Card
                    name={i.name}
                    image={i.image[2]["#text"]}
                    playcount={i.playcount}
                    listeners={i.listeners}
                    url={i.url}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default Topartistsall;
