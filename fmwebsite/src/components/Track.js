import React, { Component } from "react";
import Tracks from "../Design/tracks";
import Axios from "axios";
import Pagination from "rc-pagination";
import "rc-pagination/assets/index.css";
 class Track extends Component {
  state = {
    toptracks: [],
    current: 1
  };

  componentDidMount() {
    this.getTrackData();
  }
  onChange = async page => {
    await this.setState({
      current: page
    });
    this.getTrackData();
  };
  getTrackData() {
    Axios.get(
      "http://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=19aa554500134c46fb386b7dcef0f0b1&format=json&page=" +
        encodeURI(this.state.current)
    ).then(res => this.setState({ toptracks: res.data.tracks.track }));
  }

  render() {
    return (
      <div className="container">
        <Pagination
          hrefBuilder="#top"
          className="pagination"
          onChange={this.onChange}
          current={this.state.current}
          total={2000}
        />

        <h3>Top Tracks</h3>

        {this.state.toptracks.map((i, key) => {
          return (
            <div className="panel-body" key={key}>
              <Tracks
                name={i.name}
                image={i.image[2]["#text"]}
                playcount={i.playcount}
                duration={i.duration}
                url={i.url}
                artist={i.artist.name}
              />
            </div>
          );
        })}
      </div>
    );
  }
}

export default Track;
