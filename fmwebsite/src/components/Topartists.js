import React, { Component } from "react";
import Axios from "axios";
import Cardslider from "../Design/card";

class Topartists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topartists: [],
      term: ""
    };
    this.changeTerm = this.changeTerm.bind(this);
  }

  componentDidMount = () => {
    this.clear();
  };
  async clear() {
    this.state.term = "";
    let randomdata = ~~(Math.random() * ~~250);
    let url = `http://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&api_key=19aa554500134c46fb386b7dcef0f0b1&format=json&limit=16&page=${randomdata}`;
    await Axios.get(url).then(res =>
      this.setState({ topartists: res.data.artists.artist })
    );
  }
  async changeTerm(event) {
    if (event.target.value === "") {
     await this.clear();
    } else {
      await this.setState({ term: event.target.value });
      let url = `http://ws.audioscrobbler.com/2.0/?method=artist.search&artist=${encodeURI(
        this.state.term
      )}&api_key=19aa554500134c46fb386b7dcef0f0b1&format=json`;

      await Axios.get(url)
        .then(response => {
          this.setState({
            topartists: response.data.results.artistmatches.artist
          });
        })
        .catch(error => console.log(error));
    }
  }

  render() {
    return (
      <div className="main mt-5">
        <form>
          <input
            className="search"
            type="search"
            placeholder="Search"
            aria-label="Search"
            onChange={this.changeTerm}
          />
        </form>
        <div className="container">
          <div className="row">
            {this.state.topartists.map((i, key) => {
              return (
                <div className="col-md-3" key={key}>
                  <Cardslider
                    name={i.name}
                    image={i.image[2]["#text"]}
                    playcount={i.playcount}
                    listeners={i.listeners}
                    url={i.url}
                  />
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default Topartists;
