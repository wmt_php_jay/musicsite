import React, { Component } from "react";
import Frontpage from "./Design/navbar";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Topartists from "./components/Topartists";
import Topartistsall from "./components/Topartistsall";
import Track from "./components/Track";
import Toptags from "./components/Toptags"
class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <div>
            <Frontpage />
            <Route exact path="/" component={Topartists} />
          </div>

          <Route path="/track" component={Track} />
          <Route path="/topartistsall" component={Topartistsall} />
          <Route path="/toptags" component={Toptags} />  
        </div>
        
      </Router>
    );
  }
}

export default App;
