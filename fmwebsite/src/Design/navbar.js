import React from "react";

let frontPage = () => {
  return (
    <div>
      <nav
        className="navbar navbar-expand-lg navbar-dark fixed-top"
        id="mainNav"
      >
        <div className="container">
          <a className="navbar-brand js-scroll-trigger" href="/">
            Tune
          </a>
          <button
            className="navbar-toggler navbar-toggler-right collapsed"
            type="button"
            data-toggle="collapse"
            data-target="#navbarResponsive"
            aria-controls="navbarResponsive"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
          
            <i className="fas fa-bars" />
          </button>
          <div className="navbar-collapse collapse" id="navbarResponsive">
            <ul className="navbar-nav  ml-auto">
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/track">
                  Top Track
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/topartistsall">
                  Top Artists
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link js-scroll-trigger" href="/toptags">
                  Top Tags
                </a>
              </li>

            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default frontPage;
