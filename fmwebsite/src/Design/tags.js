import React from "react";

let tagsDesign = i => {
  let name =  i.name.charAt(0).toUpperCase() + i.name.slice(1);
  return (
    <div className="box_content">
      <ol className="rounded-list">
        <li>
          <ol>
            <li> 
              <span>
               <a href={i.url}>{name}</a>
              </span>
            </li>
          </ol>
        </li>
      </ol>
    </div>
  );
};
export default tagsDesign;
