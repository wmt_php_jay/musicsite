import React from "react";

let tracks = i => {
  return (
    <ul className="listrap">
      <li>
        <div className="listrap-toggle">
          <span />
          <a href={i.url}>
            <img alt={i.image} src={i.image} className="img-circle" />
          </a>
        </div>
        <a className="link" href={i.url}>
          <strong>{i.name}</strong>
        </a>
        <div className="play">{i.artist} </div>
        <br />
      </li>
    </ul>
  );
};

export default tracks;
