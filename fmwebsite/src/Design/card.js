import React from "react";
import notfound from "../images/notfound.png";

function convertPlayCount(listencount) {
  return Math.abs(Number(listencount)) >= 1.0e9
    ? Math.abs(Number(listencount)) / 1.0e9 + " Billions"
    : Math.abs(Number(listencount)) >= 1.0e6
    ? Math.abs(Number(listencount)) / 1.0e6 + " Millions"
    : Math.abs(Number(listencount)) >= 1.0e3
    ? Math.abs(Number(listencount)) / 1.0e3 + " K"
    : Math.abs(Number(listencount));
}

let card = i => {
  let listencount = convertPlayCount(i.playcount);
  let listeners = convertPlayCount(i.listeners);

  let images = i.image === "" ? notfound : i.image;
  return (
    <div className="artist-data mt-5">
      <a href={i.url}>
        <img src={images} alt="" className="img-responsive" />
      </a>

      <div className="artst-prfle">
        <div className="art-title">{i.name}</div>
        <div className="counter-tab">
          <div>
            <i className="fas fa-play" /> {listencount}
          </div>
          <br />
          <div>
            <i className="fas fa-headphones" /> {listeners}
          </div>
        </div>
      </div>
    </div>
  );
};

export default card;
